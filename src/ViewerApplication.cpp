#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/gltf.hpp"
#include "utils/cameras.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

// 

const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

//

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}


int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader, m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensity = glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  const auto uBaseColorTexture = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto uMetallicFactor = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uMetallicRoughnessTexture = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
 
  const auto uRoughnessFactor = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");

  const auto uEmissiveTexture = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");

  const auto uOcclusionTexture = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrength = glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uApplyOcclusion = glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");



    tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltfFile(model)) {
    return -1;
  }

  glm::vec3 bboxMin;
  glm::vec3 bboxMax;

  computeSceneBounds(model, bboxMin, bboxMax);

  // Build projection matrix
  //auto maxDistance = 500.0f; // TODO use scene bounds instead to compute this
  const auto diag = bboxMax - bboxMin;
  auto maxDistance = glm::length(diag);
  //maxDistance = maxDistance > 0.f ? maxDistance : 500.f;
 /* const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);*/
  const auto projMatrix = glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  //FirstPersonCameraController cameraController{

  /*TrackballCameraController cameraController{
      m_GLFWHandle.window(), 0.5f * maxDistance};*/

  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) 
  {
    cameraController->setCamera(m_userCamera);
  } 
  
  else 
  {

    // TODO Use scene bounds to compute a better default camera
      const auto center = 0.5f * (bboxMax + bboxMin);
      const auto up = glm::vec3(0, 1, 0);

      if (diag.z > 0) 
      {
          cameraController->setCamera(Camera{center + diag, center, up});
      }

      else
      {
          cameraController->setCamera(Camera{center + 2.f * glm::cross(diag, up), center, up});
      }

  }

  // light parameters

  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(2, 2, 2); 
  bool lightFromCamera = false;

  // occlusion 

  bool applyOcclusion = true;


  // TODO Creation of Buffer Objects

 std::vector<GLuint> bufferObjects = createBufferObjects(model);

 // Load texture

 const auto textureObjects = createTextureObjects(model);

 // white texture (if no texture) 

 GLuint whiteTexture = 0;

  // Create white texture for object with no base color texture
 
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  
  float white[] = {1, 1, 1, 1};
  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  
  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Vertex Array Objects

 std::vector<VaoRange> meshToVertexArrays;
 std::vector<GLuint> vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshToVertexArrays);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // Lambda funciton to bind material

  const auto bindMaterial = [&](const auto materialIndex) 
  {
      //Material binding
      if (materialIndex >= 0) 
      {
         const auto &material = model.materials[materialIndex];
         const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
     
          if (uBaseColorFactor >= 0) 
          {
            glUniform4f(uBaseColorFactor,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
          }

        if (uBaseColorTexture >= 0) 
        {
            auto textureObject = whiteTexture;

          if (pbrMetallicRoughness.baseColorTexture.index >= 0) 
          {
             const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
         
            if (texture.source >= 0) 
            {
              textureObject = textureObjects[texture.source];
            }
          }

           glActiveTexture(GL_TEXTURE0);
           glBindTexture(GL_TEXTURE_2D, textureObject);
           glUniform1i(uBaseColorTexture, 0);
        }

        if (uMetallicFactor >= 0)
        {
            glUniform1f(uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
        }

        if (uRoughnessFactor >= 0)
        {
             glUniform1f(uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);
        
        }

        if (uMetallicRoughnessTexture >= 0)
        {
            auto textureobject = 0u;

            if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0)
            {
                const auto& texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];

                if (texture.source >= 0)
                {
                    textureobject = textureObjects[texture.source];
                }
            }

           glActiveTexture(GL_TEXTURE1);
           glBindTexture(GL_TEXTURE_2D, textureobject);
           glUniform1i(uMetallicRoughnessTexture, 1);
        }

        if (uEmissiveFactor >= 0) 
        {
            glUniform3f(uEmissiveFactor, 
                (float)material.emissiveFactor[0],
                (float)material.emissiveFactor[1],
                (float)material.emissiveFactor[2]);
        }
         
        if (uEmissiveTexture >= 0) 
        {
            auto textureObject = 0;
        
            if (material.emissiveTexture.index >= 0) 
            {
              const auto &texture = model.textures[material.emissiveTexture.index];
              
              if (texture.source >= 0) 
              {
                textureObject = textureObjects[texture.source];
              }
            }

            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, textureObject);
            glUniform1i(uEmissiveTexture, 2);
        
        }

        if (uOcclusionStrength >= 0)
        {
            glUniform1f(uOcclusionStrength, (float)material.occlusionTexture.strength);
        }

        if (uOcclusionTexture >= 0) 
        {
            auto textureobject = whiteTexture;

            if (material.occlusionTexture.index >= 0)
            {
                const auto& texture = model.textures[material.occlusionTexture.index];
                if (texture.source >= 0)
                {
                    textureobject = textureObjects[texture.source];
                }
            }

            glActiveTexture(GL_TEXTURE3);
            glBindTexture(GL_TEXTURE_2D, textureobject);
            glUniform1i(uOcclusionTexture, 3);

        }
      } 

      else
      {
          if (uBaseColorTexture >= 0) 
          {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, whiteTexture);
            glUniform1i(uBaseColorTexture, 0);
          }

          if (uBaseColorFactor >= 0)
          {
              glUniform4f(uBaseColorFactor, 1, 1, 1, 1);
          }

          if (uMetallicFactor >= 0)
          {
              glUniform1f(uMetallicFactor, 1.f);
          }

          if (uRoughnessFactor >= 0)
          {
              glUniform1f(uRoughnessFactor, 1.f);
          }

          if (uMetallicRoughnessTexture >= 0)
          {
              glActiveTexture(GL_TEXTURE1);
              glBindTexture(GL_TEXTURE_2D, 0);
              glUniform1i(uMetallicRoughnessTexture, 1);
          }

          if (uEmissiveFactor >= 0) 
          {
             glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
          }

          if (uEmissiveTexture >= 0) 
          {
            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, 0);
            glUniform1i(uEmissiveTexture, 2);
          }

          if (uOcclusionStrength >= 0)
          {
              glUniform1f(uOcclusionStrength, 0.f);
          }

          if (uOcclusionTexture >= 0)
          {
              glActiveTexture(GL_TEXTURE3);
              glBindTexture(GL_TEXTURE_2D, 0);
              glUniform1i(uOcclusionTexture, 3);
          }
      }

  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) 
  {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // send lightIntensity to the shader
    if (uLightIntensity >= 0) {
        glUniform3f(uLightIntensity, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
    }

    if (uLightDirectionLocation >= 0) {

       /* const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0], lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);*/
      
        if (lightFromCamera) {
            glUniform3f(uLightDirectionLocation, 0, 0, 1);
        }
     
        else {
            const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
            glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0], lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
        }
    }

    // Occlusion

    if (uApplyOcclusion >= 0)
    {
        glUniform1i(uApplyOcclusion, applyOcclusion);
    }


    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive

    const std::function<void(int, const glm::mat4 &)> drawNode = [&](int nodeIdx, const glm::mat4 &parentMatrix) 
    {
          // TODO The drawNode function
          const auto &node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if (node.mesh >= 0) 
          {
            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));
          

              const auto &mesh = model.meshes[node.mesh];
              const auto &vaoRange = meshToVertexArrays[node.mesh];

              for (size_t i = 0; i < mesh.primitives.size(); i++) 
              {
                const auto vao = vertexArrayObjects[vaoRange.begin + i];
                const auto &primitive = mesh.primitives[i];

                 // bindMaterial
                bindMaterial(primitive.material);

                glBindVertexArray(vao);

                if (primitive.indices >= 0) 
                {
                  const auto &accessor = model.accessors[primitive.indices]; 
                  const auto &bufferView = model.bufferViews[accessor.bufferView]; 

                  const auto byteOffset = model.accessors[primitive.indices].byteOffset + bufferView.byteOffset;  
                  glDrawElements(primitive.mode, GLsizei(model.accessors[primitive.indices].count),
                      model.accessors[primitive.indices].componentType, (const GLvoid*) byteOffset);
                } 
                else 
                {
                  const auto accessorIdx = (*begin(primitive.attributes)).second;
                  const auto &accessor = model.accessors[accessorIdx];
                  glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
                }
              }
          }

          for (const auto childNodeIdx : node.children) {
            drawNode(childNodeIdx, modelMatrix);
          }

        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for (const auto nodeIdx : model.scenes[model.defaultScene].nodes) 
      {
        drawNode(nodeIdx , glm::mat4(1));
      }
    }
  };

  if (!m_OutputPath.empty()) {

    const auto numComponents = 3;

    std::vector<unsigned char> pixels(
        m_nWindowWidth * m_nWindowHeight * numComponents);

    renderToImage(
        m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data(), [&]() {
          const auto camera = cameraController->getCamera();
          drawScene(camera);
        });

    flipImageYAxis(
        m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight,
        numComponents, pixels.data(), 0);

    std::cout << "capture" << std::endl;

    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged = ImGui::RadioButton("Trackball", &cameraControllerType, 0) || ImGui::RadioButton("First person", &cameraControllerType, 1);
        
        if (cameraControllerTypeChanged) 
        {
            const auto currentCamera = cameraController->getCamera();
            if (cameraControllerType == 0) {
                cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
            }
            else {
                cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
            }
            cameraController->setCamera(currentCamera);
        }

      }

      // Lights

      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) 
      {
          static float lightTheta = 0.f;
          static float lightPhi = 0.f;

          if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) || ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>()))
          {
              const auto sinPhi = glm::sin(lightPhi);
              const auto cosPhi = glm::cos(lightPhi);
              const auto sinTheta = glm::sin(lightTheta);
              const auto cosTheta = glm::cos(lightTheta);

              lightDirection = glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
          }

           static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor)) 
        {
          lightIntensity = lightColor * lightIntensityFactor;
        }
        
        //Lights
        ImGui::Checkbox("Light from Camera", &lightFromCamera);
        
        // Occlusion
        ImGui::Checkbox("Apply occlusion", &applyOcclusion);

      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

// load Gltf

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn : %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }

  return true;
}

///////

// create texture object

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const
{
  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;

  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());

  // initialize each texture objects
  for (size_t i = 0; i < model.textures.size(); ++i) 
  {
    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image

    //if (texture.sampler >= 0)
    //{
    //    const auto& sampler = model.samplers[texture.sampler];
    //}
    //else 
    //{
    //    const auto& sampler =defaultSampler;
    //}

    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST || sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST || sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) 
    {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
  }

  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}


/// create buffer object
std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); i++) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObjects; 
}
  ////

///create vertex array objects 

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshToVertexArrays)
{
  std::vector<GLuint> vertexArrayObjects; 
  //const auto vaoOffset = vertexArrayObjects.size();
  meshToVertexArrays.resize(model.meshes.size()); 

  for (size_t i = 0; i < model.meshes.size(); i++) {
    
    auto &vaoRange = meshToVertexArrays[i]; // pointer to the correct index 
    vaoRange.begin = GLsizei(vertexArrayObjects.size());  // ...which is the size of vertexArrayObjects
    vaoRange.count = GLsizei(model.meshes[i].primitives.size());

    vertexArrayObjects.resize(vertexArrayObjects.size() + model.meshes[i].primitives.size());

    glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoRange.begin]);

    for (size_t j = 0; j < model.meshes[i].primitives.size(); j++) {
      const auto vao = vertexArrayObjects[vaoRange.begin + j]; // get the VAO corresponding to the primitive
      glBindVertexArray(vao);

      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0


        const auto iterator = model.meshes[i].primitives[j].attributes.find("POSITION");
      
        if (iterator != end(model.meshes[i].primitives[j].attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView];// TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)
          // const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX); 

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view

          // TODO Call glVertexAttribPointer with the correct arguments.
          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).

          glVertexAttribPointer( VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *) byteOffset);

        }
      }

      { // scope NORMAL

        const auto iterator = model.meshes[i].primitives[j].attributes.find("NORMAL");
      
        if (iterator != end(model.meshes[i].primitives[j].attributes)) { 

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer; 

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX); 
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; 

          glVertexAttribPointer( VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *) byteOffset);

        }
      }

      { // scope TEXCOORD0

        const auto iterator = model.meshes[i].primitives[j].attributes.find("TEXCOORD_0");
      
        if (iterator != end(model.meshes[i].primitives[j].attributes)) { 

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer; 

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX); 
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; 

          glVertexAttribPointer( VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), (const GLvoid *) byteOffset);

        }
      }

      if (model.meshes[i].primitives[j].indices >= 0) {
        const auto accessorIdx = model.meshes[i].primitives[j].indices;
        const auto &accessor = model.accessors[accessorIdx]; 
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer; 

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }

    }
    
  }

  glBindVertexArray(0);

  return vertexArrayObjects;

}

///

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
